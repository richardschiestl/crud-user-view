import {Component, OnInit} from '@angular/core';
import { User } from './models/user';
import { UserService } from './services/userService';

@Component({
  selector: 'user-component',
  styleUrls: ['user.component.css'],
  templateUrl: 'user.component.html',
})

export class UserComponent implements OnInit {
  users: User[] = [];

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userService.getUsers().subscribe(
      res => this.users = res      
    )
  }

}
