import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../models/user';
import { UserService } from '../services/userService';

interface Schooling {
  value: number;
  viewValue: string;
}
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  schooling: Schooling[] = [
    { value: 1, viewValue: 'Infantil' },
    { value: 2, viewValue: 'Fundamental' },
    { value: 3, viewValue: 'Médio' },
    { value: 4, viewValue: 'Superior' }
  ];

  constructor(private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar) { }

  user: User = new User;
  userForm!: FormGroup;
  maxDate: Date = new Date();
  errors: [] = [];
  clicked: boolean = false;

  async ngOnInit() {
    await this.userService.getUserById(this.route.snapshot.params['id']).then(res => this.user = res);

    this.userForm = this.formBuilder.group({
      'id': [this.user.id, Validators.required],
      'name': [this.user.name, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
      'lastName': [this.user.lastName, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
      'dateBirth': [this.user.dateBirth, Validators.required],
      'schooling': [this.user.schooling, Validators.required],
      'email': [this.user.email, Validators.required]
    });
  }

  deleteUser(): void {
    this.clicked = true;
    this.userService.deleteUser(this.userForm.value).subscribe(
      res => {
        this.openToast("Usuário excluído com sucesso!", "");
        this.router.navigate(['']);
      },
      err => {
        if (err.status === 400) {
          this.clicked = false;
          this.errors = err;
        }
      }
    );
  }

  updateUser(): void {
    this.clicked = true;
    this.userService.updateUser(this.userForm.value, this.route.snapshot.params['id']).subscribe(
      res => {
        this.openToast("Usuário alterado com sucesso!", "");
        this.router.navigate(['']);
      },
      err => {
        if (err.status === 400) {
          this.clicked = false;
          this.errors = err;
        }
      }
    );
  }

  openToast(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
