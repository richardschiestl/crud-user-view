import { Component, OnInit, ɵConsole } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { User } from '../models/user';
import { UserService } from '../services/userService';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { Router } from "@angular/router"
import { MatSnackBar } from '@angular/material/snack-bar';


interface Schooling {
  value: number;
  viewValue: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})


export class RegisterComponent implements OnInit {

  schooling: Schooling[] = [
    { value: 1, viewValue: 'Infantil' },
    { value: 2, viewValue: 'Fundamental' },
    { value: 3, viewValue: 'Médio' },
    { value: 4, viewValue: 'Superior' }
  ];

  userForm!: FormGroup;
  errors: [] = [];
  maxDate: Date = new Date();
  clicked: boolean = false;

  constructor(private userService: UserService, private formBuilder: FormBuilder, private router: Router, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      'name': [null, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
      'lastName': [null, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
      'dateBirth': [null, Validators.required],
      'schooling': [null, Validators.required],
      'email': [null, Validators.required]
    });
  }

  addUser() {
    this.clicked = true;
    this.userService.addUser(this.userForm.value).subscribe(
      res => {
        this.openToast("Usuário cadastrado com sucesso!", "");
        this.router.navigate(['']);
      },
      err => {
        if (err.status === 400) {
          this.clicked = false;
          this.errors = err;
        }
      }
    );
  }

  openToast(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
