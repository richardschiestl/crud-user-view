import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { User } from "../models/user";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const apiUrl = 'https://localhost:44306/api/user/';

@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }

    getUsers(): Observable<User[]> {
        return this.http
            .get<User[]>(apiUrl);
    }

    async getUserById(id: number): Promise<User> {
        return this.http
            .get<User>(`${apiUrl}${id}`).toPromise();
    }

    addUser(user: User): Observable<User> {
        return this.http
            .post<User>(apiUrl, user, httpOptions);
    }

    updateUser(user: User, idUser: number): Observable<User> {
        return this.http
            .put<User>(`${apiUrl}${user.id}`, user, httpOptions);
    }

    deleteUser(user: User): Observable<User> {
        return this.http
            .delete<User>(`${apiUrl}${user.id}`, httpOptions);
    }
}