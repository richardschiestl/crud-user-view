export class User {
  id!: number;
  name!: string;
  lastName!: string;
  email!: string;
  dateBirth!: Date;
  schooling!: string;
  schoolingDesc!: string;
}